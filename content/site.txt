Title: VHMaVi

----

Headerhome: Accueil

----

Headerschool: L'école

----

Headerschoolpresentation: Présentation

----

Headerschoolmission: Notre mission

----

Headerschoolstudents: Nos élèves

----

Headerschoolboardingschool: L'internat

----

Headerschoolnewsletter: Les dernières nouvelles

----

Headerwhoweare: Qui sommes-nous ?

----

Headerwhoweareteam: L'équipe

----

Headerwhowearepartners: Nos partenaires

----

Headersupport: Nous soutenir

----

Headersupportdonationssponsoring: Dons et parrainages

----

Headersupportbecomevolunteer: Devenir bénévole

----

Headercontact: Nous contacter

----

Footercontactus: Nous contacter

----

Footercontactusform: Formulaire de contact
