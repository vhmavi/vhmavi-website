<?php snippet('tete') ?>

<section class="hero">
  <div class="hero-body container">
    <h1 class="title is-spaced is-2">
      <?= $page->title()->esc() ?>
    </h1>
    <p class="subtitle">
      <?= $page->message()->kirbytextinline() ?>
    </p>
    <a href="/" class="button is-primary is-medium is-outlined">Revenir à l'accueil</a>
  </div>
</section>

<?php snippet('pied') ?>
