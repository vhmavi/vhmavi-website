<?=
snippet('head', [
  'feuillesStyleAdditionnelles' => [
    'assets/css/index/carousel.css',
    'assets/css/index/description.css',
    'assets/css/index/figures.css',
  ]
]);
?>

<!-- Carousel -->
<?= snippet('index/carousel'); ?>

<!-- Description and logo container -->
<?= snippet('index/description'); ?>

<!-- Figures container -->
<?= snippet('index/figures'); ?>

<?= snippet('footer', [
  'feuillesScriptAdditionnelles' => [
    'assets/js/index/carousel.js',
    'assets/js/index/school-figures.js',
  ]
]) ?>
