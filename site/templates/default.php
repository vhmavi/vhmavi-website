<?php snippet('tete') ?>

<section class="hero">
  <div class="hero-body container">
    <h1 class="title is-spaced is-2">
      <?= $page->title()->esc() ?>
    </h1>
  </div>
</section>

<?php snippet('pied') ?>
