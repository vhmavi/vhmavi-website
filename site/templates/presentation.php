<?=
snippet('head', [
  'feuillesStyleAdditionnelles' => [
    'assets/css/vhmavi-txt-img-shared-row.css'
  ]
]);
?>

<!-- Contexte -->
<div class="container vhmavi-container vhmavi-text-img-shared-container">
    <div class="row row-cols-1 row-cols-lg-2 vhmavi-txt-img-shared-row">
        <div class="col vhmavi-text-col vhmavi-text-col-left">
            <h1 class="vhmavi-presentation-h1"><?= $page->Paragraph1header()->esc() ?></h1>
            <p><?= $page->Paragraph1text1()->esc() ?></p>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph1text2() ?></p>
            <p><?= $page->Paragraph1text3()->esc() ?></p>
        </div>
        <div class="col vhmavi-img-col vhmavi-img-col-right">
            <img src="img/presentation/nepal_field.jpg" alt="<?= $page->Imgalt1()->esc() ?>" id="vhmavi-img-nepal-field" />
        </div>
    </div>
    <div class="row row-cols-1 row-cols-lg-2 vhmavi-txt-img-shared-row">
        <div class="col vhmavi-img-col vhmavi-img-col-left order-12 order-lg-1">
            <img src="img/presentation/little_student.jpg" alt="<?= $page->Imgalt2()->esc() ?>" id="vhmavi-img-little-student" />
        </div>
        <div class="col vhmavi-text-col vhmavi-text-col-right order-1 order-lg-12">
            <h2 class="vhmavi-presentation-h2"><?= $page->Paragraph2header()->esc() ?></h2>
            <p><?= $page->Paragraph2text1()->esc() ?></p>
            <p><?= $page->Paragraph2text2()->esc() ?></p>
            <p><?= $page->Paragraph2text3()->esc() ?></p>
            <p><?= $page->Paragraph2text4()->esc() ?></p>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-lg-2 vhmavi-txt-img-shared-row">
        <div class="col vhmavi-text-col vhmavi-text-col-left">
            <h2 class="vhmavi-presentation-h2"><em><?= $page->Paragraph3header()->esc() ?></em></h2>
            <p><?= $page->Paragraph3text1()->esc() ?></p>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph3text2() ?></p>
            <p><span class="vhmavi-school-name">Victor Hugo</span><?= $page->Paragraph3text3()->esc() ?></p>
            <p><span class="vhmavi-school-name">Manjushree</span><?= $page->Paragraph3text4()->esc() ?></p>
            <p><span class="vhmavi-school-name">Vidyapith</span><?= $page->Paragraph3text5()->esc() ?></p>
            <p><?= $page->Paragraph3text6()->esc() ?></p>
        </div>
        <div class="col vhmavi-img-col vhmavi-img-col-right">
            <img src="img/presentation/old_school.jpg" alt="<?= $page->Imgalt3()->esc() ?>" />
        </div>
    </div>
    <div class="row row-cols-1 row-cols-lg-2 vhmavi-txt-img-shared-row">
        <div class="col vhmavi-img-col vhmavi-img-col-left order-12 order-lg-1">
            <img src="img/presentation/preschool_building.jpg" alt="<?= $page->Imgalt4()->esc() ?>" id="vhmavi-img-preschool-building" />
        </div>
        <div class="col vhmavi-text-col vhmavi-text-col-right order-1 order-lg-12">
            <h2 class="vhmavi-presentation-h2"><?= $page->Paragraph4header()->esc() ?></h2>
            <p><?= $page->Paragraph4text1()->esc() ?></p>
            <p><?= $page->Paragraph4text2()->esc() ?></p>
            <p><?= $page->Paragraph4text3()->esc() ?></p>
            <p><?= $page->Paragraph4text4()->esc() ?></p>
        </div>
    </div>
</div>

<?= snippet('footer', [
  'feuillesScriptAdditionnelles' => [
    'assets/js/index/school-figures.js',
    'assets/js/vhmavi-txt-img-shared-row.js',
  ]
]) ?>
