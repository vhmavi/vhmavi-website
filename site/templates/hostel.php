<?=
snippet('head', [
  'feuillesStyleAdditionnelles' => [
    'assets/css/vhmavi-full-width-img.css'
  ]
]);
?>

<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/hostel/hostel_students.jpg" alt="<?= $page->Imgalt1()->esc() ?>" id="vhmavi-img-hostel-students" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h1><?= $page->Paragraph1header()->esc() ?></h1>
            <p><?= $page->Paragraph1text1()->esc() ?></p>
            <p><?= $page->Paragraph1text2()->esc() ?></p>
            <p><?= $page->Paragraph1text3()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/hostel/hostel.jpg" alt="<?= $page->Imgalt2()->esc() ?>" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph2header()->esc() ?></h2>
            <p><?= $page->Paragraph2text1()->esc() ?></p>
            <p><?= $page->Paragraph2text2()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/hostel/new_hostel.jpg" alt="<?= $page->Imgalt3()->esc() ?>" id="vhmavi-img-new-hostel" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph3header()->esc() ?></h2>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph3text1() ?></p>
        </div>
    </div>
</div>

<?= snippet('footer') ?>
