<?=
snippet('head', [
  'feuillesStyleAdditionnelles' => [
    'assets/css/vhmavi-full-width-img.css',
    'assets/css/vhmavi-icon.css'
  ]
]);
?>

<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h1><?= $page->Paragraph1header()->esc() ?></h1>
            <p><?= $page->Paragraph1text1()->esc() ?></p>
            <p><?= $page->Paragraph1text2()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/mission/student_dance.jpg" alt="<?= $page->Imgalt1()->esc() ?>" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph2header()->esc() ?></h2>
            <p><?= $page->Paragraph2text1()->esc() ?></p>
            <p><?= $page->Paragraph2text2()->esc() ?></p>
        </div>
        <div class="col-12 col-md-6 col-lg-4 vhmavi-icons-text-col">
            <div class="vhmavi-icon-container">
                <i class="vhmavi-icon-paint-palette"></i>
            </div>
            <p><?= $page->Paragraph2text3()->esc() ?></p>
        </div>
        <div class="col-12 col-md-6 col-lg-4 vhmavi-icons-text-col">
            <div  class="vhmavi-icon-container">
                <i class="vhmavi-icon-speech-bubble"></i>
            </div>
            <p><?= $page->Paragraph2text4()->esc() ?></p>
        </div>
        <div class="col-12 vhmavi-icons-text-col vhmavi-icons-text-col-last">
            <div class="vhmavi-icon-container">
                <i class="vhmavi-icon-knowledge"></i>
            </div>
            <p><?= $page->Paragraph2text5()->esc() ?></p>
        </div>
        <div class="col-12 col-lg-9">
            <p><?= $page->Paragraph2text6()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/mission/student_reuse.jpg" alt="<?= $page->Imgalt2()->esc() ?>" id="vhmavi-img-student-reuse" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph3header()->esc() ?></h2>
            <p><?= $page->Paragraph3text1()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/mission/student_group.jpg" alt="<?= $page->Imgalt3()->esc() ?>" id="vhmavi-img-student-group" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph4header()->esc() ?></h2>
            <p><?= $page->Paragraph4text1()->esc() ?></p>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph4text2() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/mission/preschool_student.jpg" alt="<?= $page->Imgalt4()->esc() ?>" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph5header()->esc() ?></h2>
            <p><?= $page->Paragraph5text1()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/mission/teacher.jpg" alt="<?= $page->Imgalt5()->esc() ?>" id="vhmavi-img-teacher" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2><?= $page->Paragraph6header()->esc() ?></h2>
            <p><?= $page->Paragraph6text1()->esc() ?></p>
            <p><?= $page->Paragraph6text2()->esc() ?></p>
            <p><?= $page->Paragraph6text3()->esc() ?></p>
        </div>
    </div>
</div>

<?= snippet('footer', [
  'feuillesScriptAdditionnelles' => [
    'assets/js/index/school-figures.js',
  ]
]) ?>
