<?=
snippet('head', [
  'feuillesStyleAdditionnelles' => [
    'assets/css/vhmavi-full-width-img.css',
    'assets/css/vhmavi-txt-img-shared-row.css'
  ]
]);
?>

<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h1><?= $page->Paragraph1header()->esc() ?></h1>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph1text1() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-text-img-shared-container">
    <div class="row row-cols-1 row-cols-lg-2 vhmavi-txt-img-shared-row">
        <div class="col vhmavi-text-col vhmavi-text-col-left">
            <h2 class="vhmavi-students-h2"><?= $page->Paragraph2header()->esc() ?></h2>
            <!-- FIXME: No escape -->
            <p><?= $page->Paragraph2text1() ?></p>
        </div>
        <div class="col vhmavi-img-col vhmavi-img-col-right">
            <img src="img/students/student_smile.jpg" alt="<?= $page->Imgalt1()->esc() ?>" id="vhmavi-img-student-smile" />
        </div>
    </div>
</div>
<div class="container vhmavi-container">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-9">
            <h2 class="vhmavi-students-h2"><?= $page->Paragraph3header()->esc() ?></h2>
            <p><?= $page->Paragraph3text1()->esc() ?></p>
            <p><?= $page->Paragraph3text2()->esc() ?></p>
            <p><?= $page->Paragraph3text3()->esc() ?></p>
        </div>
    </div>
</div>
<div class="container vhmavi-container vhmavi-full-width-img-container">
    <div class="row">
        <div class="col vhmavi-img-col">
            <img src="img/students/student_group.jpg" alt="<?= $page->Imgalt2()->esc() ?>" id="vhmavi-img-student-group" />
        </div>
    </div>
</div>

<?= snippet('footer', [
  'feuillesScriptAdditionnelles' => [
    'assets/js/index/school-figures.js',
    'assets/js/vhmavi-txt-img-shared-row.js',
  ]
]) ?>
