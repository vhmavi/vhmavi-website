<!--
    index-description.php
    Description of the school and its logo used in index.php
 -->

<div class="container vhmavi-container vhmavi-description-container">
    <div class="row justify-content-center">
        <!--
            Description container - on medium, small and extra-small devices, it takes
            full screen width
        -->
        <div class="col-12 col-lg-9">
            <h1><?= $page->descriptionheader()->esc() ?></h1>
            <h2><?= $page->descriptionheaderslogan()->esc() ?></h2>
            <!-- FIXME: No escape -->
            <p><?= $page->descriptiondescription1() ?></p>
            <p><?= $page->descriptiondescription2()->esc() ?></p>
            <p><?= $page->descriptiondescription3()->esc() ?></p>
        </div>
        <!--
            Logo container - on medium, small and extra-small devices, it takes
            full screen width
        -->
        <div class="col-12 col-lg-5 vhmavi-centered-col">
            <img src="img/index/logo_name.svg" alt="<?= $site->Descriptionimgalt1()->esc() ?>" />
        </div>
    </div>
</div>
