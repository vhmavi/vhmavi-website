<hr class="vhmavi-divider">
<div class="container vhmavi-footer-container">
  <div class="row">
    <!-- Address column - full screen width on extra-small devices -->
    <div class="col-12 col-sm-5 col-md-4 vhmavi-address-col">
      <p>Victor Hugo Manjushree Vidyapith</p>
      <p>Chapali-Bhangal</p>
      <p>Budhanilkantha Municipality - 2</p>
      <p>Katmandou, Népal</p>
    </div>
    <!-- Contact column -->
    <div class="col col-sm">
      <p class="vhmavi-contact-title"><?= $site->footercontactus()->esc() ?></p>
      <p><a href="contact.php"><?= $site->footercontactusform()->esc() ?></a></p>
      <p>+977 01 437 94 69</p>
    </div>
    <!-- Facebook logo and link column -->
    <div class="col-3 col-sm-2 col-md-1">
      <div class="vhmavi-facebook-logo-container">
        <a href="https://www.facebook.com/vhmav/" target="_blank" rel="noopener noreferer" aria-label="VHMaVi Facebook page">
          <i class="vhmavi-icon-facebook">
            <span class="path1"></span>
            <span class="path2"></span>
          </i>
        </a>
      </div>
    </div>
  </div>
</div>

<?php
$feuillesScriptBase = [
  'assets/js/jquery.js',
  'assets/js/bootstrap.js',
  '@auto'
];
$feuillesScript = isset($feuillesScriptAdditionnelles) ? array_merge($feuillesScriptBase, $feuillesScriptAdditionnelles) : $feuillesScriptBase;
?>
<?= js($feuillesScript) ?>

<?php snippet('matomo'); ?>

</body>

</html>
