<?php
$seoDescription = $page->description();
$hasSeoDescription = $seoDescription->isNotEmpty();

if ($hasSeoDescription) :
?>
  <meta name="description" content="<?= $page->description() ?>">
<?php endif ?>

<meta property="og:title" content="<?= $page->title() ?>" />
<meta property="og:site_name" content="<?= $site->title() ?>" />
<meta property="og:url" content="<?= $page->url() ?>" />
<meta property="og:locale" content="fr_FR">
<meta property="og:type" content="website" />
<?php if ($hasSeoDescription) : ?>
  <meta property="og:description" content="<?= $seoDescription ?>" />
<?php endif;

if ($logo = $site->logoAvecMarge()->toFile()) : ?>
  <meta property="og:image" content="<?= $logo->url() ?>">
<?php endif; ?>
<meta property="og:image:alt" content="<?= $site->title() ?>">
<meta property="og:image:type" content="image/jpeg">
<meta property="og:image:width" content="1201">
<meta property="og:image:height" content="631">

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@annso_" />
<meta name="twitter:creator" content="@annso_" />
<meta name="twitter:title" content="<?= $page->title() ?>" />
<?php if ($hasSeoDescription) : ?>
  <meta name="twitter:description" content="<?= $page->description() ?>" />
<?php endif;

if ($logo) :
?>
  <meta name="twitter:image" content="<?= $logo->url() ?>">
<?php endif ?>
