<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="referrer" content="origin-when-cross-origin">

  <?php
  /*
    Prevent image-scrapping tools for AI
    See https://neil-clarke.com/block-the-bots-that-feed-ai-models-by-scraping-your-website/
  */
  ?>
  <meta name="robots" content="noai, noimageai">

  <title><?= $site->title()->esc() ?> | <?= $page->title()->esc() ?></title>

  <!-- TODO: -->
  <?php snippet('head-seo') ?>

  <?php
  $feuillesStyleBase = [
    'assets/css/bootstrap.css',
    'assets/css/index.css',
    'assets/css/tete.css',
    'assets/css/pied.css',
    '@auto'
  ];
  $feuillesStyle = isset($feuillesStyleAdditionnelles) ? array_merge($feuillesStyleBase, $feuillesStyleAdditionnelles) : $feuillesStyleBase;
  ?>
  <?= css($feuillesStyle) ?>

  <!-- TODO: -->
  <?php snippet('favicon') ?>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <!-- The logo and brand name -->
    <a class="navbar-brand" href="index">
      <img src="img/logo.svg" width="64" height="64" alt="logo">
      Victor Hugo Manjushree Vidyapith
    </a>

    <!-- For narrow screens : navbar is changed to a button and an accordion menu -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- The different menus - uses collapse so its aspect change on narrow screens (accordion menu) -->
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <!--
                Allows to activate the link in menu only if it is the active page
                So if we are on /index or / links, the corresponding menu "Accueil" will be
                active (yellow)
            -->
        <?php
        if (
          $_SERVER['REQUEST_URI'] == '/index'
          || $_SERVER['REQUEST_URI'] == '/'
        ) :
        ?>
          <li class="nav-item active">
          <?php else : ?>
          <li class="nav-item">
          <?php endif ?>
          <a class="nav-link" href="/">
            <?= $site->headerhome()->esc() ?>
          </a>
          </li>
          <?php
          if (
            $_SERVER['REQUEST_URI'] == '/presentation'
            || $_SERVER['REQUEST_URI'] == '/mission'
            || $_SERVER['REQUEST_URI'] == '/students'
            || $_SERVER['REQUEST_URI'] == '/hostel'
            || $_SERVER['REQUEST_URI'] == '/news'
          ) :
          ?>
            <li class="nav-item dropdown active vhmavi-show">
            <?php else : ?>
            <li class="nav-item dropdown vhmavi-show">
            <?php endif ?>
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?= $site->headerschool()->esc() ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?php if ($_SERVER['REQUEST_URI'] == '/presentation') : ?>
                <a class="dropdown-item vhmavi-active" href="presentation">
                <?php else : ?>
                  <a class="dropdown-item" href="presentation">
                  <?php endif ?>
                  <?= $site->headerschoolpresentation()->esc() ?>
                  </a>
                  <div class="dropdown-divider"></div>
                  <?php if ($_SERVER['REQUEST_URI'] == '/mission') : ?>
                    <a class="dropdown-item vhmavi-active" href="mission">
                    <?php else : ?>
                      <a class="dropdown-item" href="mission">
                      <?php endif ?>
                      <?= $site->headerschoolmission()->esc() ?>
                      </a>
                      <div class="dropdown-divider"></div>
                      <?php if ($_SERVER['REQUEST_URI'] == '/students') : ?>
                        <a class="dropdown-item vhmavi-active" href="students">
                        <?php else : ?>
                          <a class="dropdown-item" href="students">
                          <?php endif ?>
                          <?= $site->headerschoolstudents()->esc() ?>
                          </a>
                          <div class="dropdown-divider"></div>
                          <?php if ($_SERVER['REQUEST_URI'] == '/hostel') : ?>
                            <a class="dropdown-item vhmavi-active" href="hostel">
                            <?php else : ?>
                              <a class="dropdown-item" href="hostel">
                              <?php endif ?>
                              <?= $site->headerschoolboardingschool()->esc() ?>
                              </a>
                              <div class="dropdown-divider"></div>
                              <?php if ($_SERVER['REQUEST_URI'] == '/news') : ?>
                                <a class="dropdown-item vhmavi-active" href="news">
                                <?php else : ?>
                                  <a class="dropdown-item" href="news">
                                  <?php endif ?>
                                  <?= $site->headerschoolnewsletter()->esc() ?>
                                  </a>
            </div>
            </li>
            <?php
            if (
              $_SERVER['REQUEST_URI'] == '/team'
              || $_SERVER['REQUEST_URI'] == '/sponsors'
            ) :
            ?>
              <li class="nav-item dropdown active vhmavi-show">
              <?php else : ?>
              <li class="nav-item dropdown vhmavi-show">
              <?php endif ?>
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $site->headerwhoweare()->esc() ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php if ($_SERVER['REQUEST_URI'] == '/team') : ?>
                  <a class="dropdown-item vhmavi-active" href="team">
                  <?php else : ?>
                    <a class="dropdown-item" href="team">
                    <?php endif ?>
                    <?= $site->headerwhoweareteam()->esc() ?>
                    </a>
                    <div class="dropdown-divider"></div>
                    <?php if ($_SERVER['REQUEST_URI'] == '/sponsors') : ?>
                      <a class="dropdown-item vhmavi-active" href="sponsors">
                      <?php else : ?>
                        <a class="dropdown-item" href="sponsors">
                        <?php endif ?>
                        <?= $site->headerwhowearepartners()->esc() ?>
                        </a>
              </div>
              </li>
              <?php
              if (
                $_SERVER['REQUEST_URI'] == '/donation-sponsor'
                // || $_SERVER['REQUEST_URI'] == '/become-sponsor' // TODO : Uncomment when become-sponsor is ready
                || $_SERVER['REQUEST_URI'] == '/volunteer'
              ) :
              ?>
                <li class="nav-item dropdown active vhmavi-show">
                <?php else : ?>
                <li class="nav-item dropdown vhmavi-show">
                <?php endif ?>
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?= $site->headersupport()->esc() ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <?php if ($_SERVER['REQUEST_URI'] == '/donation-sponsor') : ?>
                    <a class="dropdown-item vhmavi-active" href="donation-sponsor">
                    <?php else : ?>
                      <a class="dropdown-item" href="donation-sponsor">
                      <?php endif ?>
                      <?= $site->headersupportdonationssponsoring()->esc() ?>
                      </a>
                      <div class="dropdown-divider"></div>
                      <!-- TODO : Uncomment when become-sponsor is ready -->
                      <!-- <?php if ($_SERVER['REQUEST_URI'] == '/become-sponsor') : ?>
                    <a class="dropdown-item vhmavi-active" href="become-sponsor">
                    <?php else : ?>
                    <a class="dropdown-item" href="become-sponsor">
                    <?php endif ?>
                        Devenir partenaire
                    </a>
                    <div class="dropdown-divider"></div> -->
                      <?php if ($_SERVER['REQUEST_URI'] == '/volunteer') : ?>
                        <a class="dropdown-item vhmavi-active" href="volunteer">
                        <?php else : ?>
                          <a class="dropdown-item" href="volunteer">
                          <?php endif ?>
                          <?= $site->headersupportbecomevolunteer()->esc() ?>
                          </a>
                </div>
                </li>
                <?php if ($_SERVER['REQUEST_URI'] == '/contact') : ?>
                  <li class="nav-item active">
                  <?php else : ?>
                  <li class="nav-item">
                  <?php endif ?>
                  <a class="nav-link" href="contact">
                    <?= $site->headercontact()->esc() ?>
                  </a>
                  </li>
      </ul>
    </div>
  </nav>
