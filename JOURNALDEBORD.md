# 🧭 Journal de bord

Ce journal de bord a pour vocation de faire état de l'avancement du projet. Il ne s'agit pas de notes de version ni d'une [historique de _commits_ d'outils de versionnage](https://github.com/conventional-changelog/standard-version), notamment en raison des différences suivantes :

-   il rend transparent aux autres parties prenantes du projet le travail effectué par l'équipe technique, dans un soucis de vulgarisation et de transparence ;
-   il permet à l'équipe technique de prendre le temps de digérer le travail effectué ;
-   il tient une compatibilité des crédits consommés sur le projet ;
-   il permet de suivre l'évolution de la motivation des troupes ;
-   il peut servir de prises de notes des difficultés rencontrées et de leurs solutions employées, à la manière d'un journal scientifique.

Ce journal de bord est grandement inspiré des explorations faites par [David Larlet](https://larlet.fr) et partagées dans son article [**Traces**](https://larlet.fr/david/2022/12/19/).

## 20 mars 2024

### @mlbiche

⏰ _crédits consommé : 0.5_
